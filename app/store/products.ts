import {createAsyncThunk} from '@reduxjs/toolkit';

import {FETCH_LIST_REQUEST,} from './actionTypes';
import {useMutation} from "@tanstack/react-query";
import {getList} from "../../api";

interface MyState {
    list: object[]; // Example property for a list of strings
}

const initialState: MyState = {
    list: [],
};

export const getProducts = createAsyncThunk<any[], undefined, { rejectValue: string }>(
    FETCH_LIST_REQUEST,
    async (_, thunkAPI) => {
        // const {data, error, isPending, isSuccess, mutateAsync} = useMutation({
        //     mutationFn: getList
        // });

        try {
            const {data} = await getList(); // Trigger the mutation

            initialState.list = data.list
            // const response = await getList();
            return data;
        } catch (error) {
            return thunkAPI.rejectWithValue('Failed to fetch list');
        }
    }
);