'use client'

import {QueryClient, QueryClientProvider} from '@tanstack/react-query'
import React from "react";

export default function ReactQueryProvider(item: any) {
    const [queryClient] = React.useState(() => new QueryClient())

    return (
        <QueryClientProvider client={queryClient}>{item.children}</QueryClientProvider>
    )
}