module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",

    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {

    fontSize: {
      xxs: '0.5rem',
      xs: '0.75rem',
      sm: '0.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
    },
    extend: {
      keyframes: {
        bounceSlow: {
          '0%, 100%': {transform: 'translate3d(0px, 0px, 0px)'},
          '50%': {transform: 'translate3d(0px, -80px, 0px)'},
        }
      },

      animation: {
        'bounce-slow': 'bounceSlow 7s linear infinite',
      },

      colors: {
        'primary': {
          100: '#46B666',
          200: '#54B990',
          500: '#499b7a80',
          600: '#DDF1E9',
          700: '#F6F6F6',
          800: '#BBE3D3'
        },
        'secondary': {
          200: '#7C085E'
        },
        'black': {
          100: '#19191A',
          200: '#656565',
          300: '#232323',
          400: '#2d2d2d',
          500: '#484848',
          600: '#2D2D2D'
        },
        'white': {
          100: '#FFFFFF',
          200: '#F1F1F1',
          300: '#E1E1E1',
          400: '#ADA8A8',
          500: '#C6C6C6',
        },
        'danger': {
          200: '#FF0000',
        },

        'error': {
          100: '#FF4C4C1A',
          200: '#F89595',
          300: '#F16363',
          400: '#DC2E2E',
          500: '#BC0000',
          600: '#950000',
          700: '#680000',
        },

        'success': {
          100: '#BFF0BE',
          200: '#78D876',
          300: '#54CC52',
          400: '#30C02D',
          500: '#30C02D',
          600: '#037C00',
          700: '#035101',
        },

        'warning': {
          100: '#FFB1141A',
          200: '#FFB114',
          300: '#CC9D27',
          400: '#C08A00',
          500: '#AE7D00',
          600: '#9C7000',
          700: '#393222',
        },
        'information': {
          100: '#D7E7FF',
          200: '#77ADFF',
          300: '#4790FF',
          400: '#1673FF',
          500: '#0B59CF',
          600: '#003F9E',
          700: '#002B6B',
        },

      },
    },
  },
  plugins: [],
}