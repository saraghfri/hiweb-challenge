'use client'

import {useRouter} from "next/navigation";
import {useEffect} from "react";

export default function Page() {
    const router = useRouter();

    useEffect(() => {
        document.title = 'صفحه اصلی'
        router.push('/login');
    }, [router]);

    return (
        <div className="flex flex-col w-full bg-white-300 dark:bg-black-100">
            <section
                className="flex items-center justify-center fixed bottom-0 bg-white-100 h-full w-full text-right inset-0 ">
                <h1 className="text-lg text-center">به های وب خوش آمدید.</h1>
            </section>
        </div>
    )
}