import React from 'react';
import Link from 'next/link';

interface PaginationProps {
    currentPage: number;
    totalPages: number;
    onPageChange: (pageNumber: number) => void;
}

function Pagination({ currentPage, totalPages, onPageChange }: PaginationProps) {
    const handlePageChange = (pageNumber: number) => {
        onPageChange(pageNumber);
    };

    const pageLinks: React.ReactElement[] = [];
    for (let i = 1; i <= totalPages; i++) {
        pageLinks.push(
            <li key={i} className={`flex items-center justify-center h-[35px] w-[35px] rounded-md text-gray-700 hover:bg-gray-200 ${currentPage === i ? 'bg-gray-300 text-white' : ''}`}>
                <Link href={`?page=${i}`}>
                    <span onClick={() => handlePageChange(i)}>{i}</span>
                </Link>
            </li>
        );
    }

    const hasPrev = currentPage > 1;
    const hasNext = currentPage < totalPages;

    return (
        <div className="pagination flex justify-center items-center mt-4">
            <button
                disabled={!hasPrev}
                onClick={() => handlePageChange(currentPage - 1)}
                className={`border border-[#C9C9C9] disabled:opacity-50 flex items-center justify-center h-[35px] w-[35px] rounded-md text-gray-700 hover:bg-gray-200 ${hasPrev ? 'bg-transparent' : 'bg-gray-300 font-bold text-white'}`}
            >
                <svg  height="15" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M1.03683 1.21352C0.972354 1.15344 0.92064 1.08099 0.884771 1.00049C0.848903 0.919988 0.829617 0.833089 0.828063 0.744973C0.826508 0.656858 0.842717 0.569333 0.875723 0.487619C0.908728 0.405904 0.957855 0.331674 1.02017 0.269359C1.08249 0.207042 1.15672 0.157914 1.23843 0.12491C1.32015 0.0919028 1.40767 0.0756942 1.49579 0.0772487C1.5839 0.0788032 1.6708 0.0980903 1.7513 0.133958C1.8318 0.169826 1.90425 0.22154 1.96433 0.286016L10.7143 9.03602C10.8372 9.15906 10.9063 9.32586 10.9063 9.49977C10.9063 9.67367 10.8372 9.84047 10.7143 9.96352L1.96433 18.7135C1.83993 18.8294 1.67538 18.8925 1.50537 18.8895C1.33536 18.8865 1.17315 18.8177 1.05291 18.6974C0.932673 18.5772 0.8638 18.415 0.860801 18.245C0.857801 18.075 0.920908 17.9104 1.03683 17.786L9.32198 9.49977L1.03683 1.21352Z"
                        fill="#000"/>
                </svg>

            </button>
            <ul className="flex space-x-2 mx-2">{pageLinks}</ul>
            <button
                disabled={!hasNext}
                onClick={() => handlePageChange(currentPage + 1)}
                className={`border border-[#C9C9C9] disabled:opacity-50 flex items-center justify-center h-[35px] w-[35px] rounded-md text-gray-700 hover:bg-gray-200 ${hasNext ? 'bg-transparent' : 'bg-gray-300 font-bold text-white'}`}
            >
                <svg className='transform rotate-180'
                    height="15" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M1.03683 1.21352C0.972354 1.15344 0.92064 1.08099 0.884771 1.00049C0.848903 0.919988 0.829617 0.833089 0.828063 0.744973C0.826508 0.656858 0.842717 0.569333 0.875723 0.487619C0.908728 0.405904 0.957855 0.331674 1.02017 0.269359C1.08249 0.207042 1.15672 0.157914 1.23843 0.12491C1.32015 0.0919028 1.40767 0.0756942 1.49579 0.0772487C1.5839 0.0788032 1.6708 0.0980903 1.7513 0.133958C1.8318 0.169826 1.90425 0.22154 1.96433 0.286016L10.7143 9.03602C10.8372 9.15906 10.9063 9.32586 10.9063 9.49977C10.9063 9.67367 10.8372 9.84047 10.7143 9.96352L1.96433 18.7135C1.83993 18.8294 1.67538 18.8925 1.50537 18.8895C1.33536 18.8865 1.17315 18.8177 1.05291 18.6974C0.932673 18.5772 0.8638 18.415 0.860801 18.245C0.857801 18.075 0.920908 17.9104 1.03683 17.786L9.32198 9.49977L1.03683 1.21352Z"
                        fill="#000"/>
                </svg>

            </button>
        </div>
    );
}

export default Pagination;


// import React from "react";
//
// interface props {
//     maxVisibleButtons: number,
//     totalPages: number,
//     total: number,
//     perPage: number,
//     currentPage: number,
//     hasMorePages: boolean,
// }
//
// const Pagination: React.FC<props> = ({
//                                          maxVisibleButtons= 2,
//                                          totalPages,
//                                          total,
//                                          perPage,
//                                          currentPage,
//                                          hasMorePages
//                                      }) => {
//
//     const onClickFirstPage = () => {
//         // emit('pagechanged', 1)
//     }
//
//     const onClickPreviousPage = () => {
//         // emit('pagechanged', props.currentPage - 1)
//     }
//
//     const onClickPage = (page) => {
//         // emit('pagechanged', page)
//     }
//
//     const onClickNextPage = () => {
//         // emit('pagechanged', props.currentPage + 1)
//     }
//
//     const onClickLastPage = () => {
//         // emit('pagechanged', props.totalPages)
//     }
//     const isPageActive = (page:number) => {
//         return currentPage === page;
//     }
//
//     const startPage = () => {
//         if (currentPage === 1) {
//             return 1;
//         }
//
//         if (currentPage === totalPages) {
//             return totalPages - maxVisibleButtons + 1;
//         }
//
//         return currentPage - 1;
//     }
//
//     const endPage = () => {
//         return Math.min(
//             startPage() + maxVisibleButtons - 1,
//             totalPages
//         );
//     }
//
//     const pages = () => {
//         const range = [];
//
//         for (let i = startPage(); i <= endPage(); i += 1) {
//             range.push({
//                 name: i,
//                 isDisabled: i === currentPage
//             });
//         }
//
//         return range;
//     }
//
//     const isInFirstPage = () => {
//         return currentPage === 1;
//     }
//
//     const isInLastPage = () => {
//         return currentPage === totalPages;
//     }
//
//     return(
//
//         <div className="flex justify-center">
//             <ul className="pagination bg-white p-2 rounded flex items-center md:overflow-hidden overflow-x-auto md:max-w-full max-w-xs">
//                 <li className="pagination-item">
//                     <button className=`${'cursor-not-allowed': isInFirstPage} rounded-r rounded-sm border border-gray-100 px-3 py-2 hover:bg-gray-100 hover:dark:text-black-100 text-black-100 dark:text-white-100 no-underline`
//                    onClick={onClickFirstPage}
//                     :disabled="isInFirstPage"
//                     >&laquo;</button>
//             </li>
//
//             <li className="pagination-item">
//                 <button
//                     type="button"
//                 @click="onClickPreviousPage"
//                 :disabled="isInFirstPage"
//                 className-label="Go to previous page"
//                 className="rounded-sm border border-gray-100 px-3 py-2 hover:bg-gray-100 hover:dark:text-black-100
//                 text-black-100 dark:text-white-100 no-underline mx-2 text-sm"
//                 :className="{'cursor-not-allowed': isInFirstPage}"
//                 >قبلی
//             </button>
//         </li>
//
//     <li
//         v-for="page in pages"
//         className="pagination-item"
//     :key="page.name"
//         >
//         <span
//     className="rounded-sm border border-primary-100 px-3 py-2 bg-primary-500 text-black-100 dark:text-white-100 no-underline cursor-not-allowed mx-2"
//     v-if="isPageActive(page.name)"
//         >{{page.name}}</span>
//     <a
//         className="rounded-sm border border-gray-100 px-3 py-2 hover:bg-gray-100 hover:dark:text-black-100 text-black-100 dark:text-white-100 no-underline mx-2"
//         href="#"
//         v-else
//     @click.prevent="onClickPage(page.name)"
//     role="button"
//         >{{page.name}}</a>
// <!-- <button
//         type="button"
//           @click = "onClickPage(page.name)"
// :
//     disabled = "page.isDisabled"
// :
//     className = "{ active: isPageActive(page.name) }"
//         > {
//     {
//         page.name
//     }
// }</button>
//     -- >
//     < /li>
//
//     <li className="pagination-item">
//         <button
//             type="button"
//         @click="onClickNextPage"
//         :disabled="isInLastPage"
//         aria-label="Go to next page"
//         className="rounded-sm border border-gray-100 px-3 py-2 hover:bg-gray-100 hover:dark:text-black-100 text-black-100
//         dark:text-white-100 no-underline mx-2 text-sm"
//         :className="{'cursor-not-allowed': isInLastPage}"
//         >بعدی
//     </button>
// </li>
//
//     <li className="pagination-item">
//         <button
//             className="rounded-r rounded-sm border border-gray-100 px-3 py-2 hover:bg-gray-100 hover:dark:text-black-100 text-black-100 dark:text-white-100 no-underline"
//         :className="{'cursor-not-allowed': isInLastPage}"
//         :disabled="isInLastPage"
//         @click.prevent="onClickLastPage"
//         rel="next"
//         role="button"
//         >&raquo;</button>
// </li>
// </ul>
// </div>
//
//     )
//
// }
//
// export default Pagination;