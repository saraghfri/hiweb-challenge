"use client"

import React, {useEffect, useState} from "react";

import Input from "./base/Input";

import {useMutation} from "@tanstack/react-query";
import {addProduct} from "../api";
import {useRouter} from "next/navigation";

import  {setCookie , deleteCookie} from "cookies-next";

function Navbar() {
    const [userName , setUserName] = useState<string>('')
    useEffect(() => {
        if(localStorage.getItem('userData') && JSON.parse(localStorage.getItem('userData'))?.userName){
            setUserName(JSON.parse(localStorage.getItem('userData'))?.userName)
        }
    }, []);
    const router = useRouter();

    const [formData, setFormData] = useState({
        ProductTitle: '',
        ProductPrice: 0,
        Description: '',
        file: {}
    });

    const [errorMessage, setErrorMessage] = useState<string | null>(null);
    const [successMessage, setSuccessMessage] = useState<string | null>(null);
    const [isSuccess, setIsSuccess] = useState<boolean>(false);

    const [nameProps, setNameProps] = useState<object>({
        label: 'نام محصول',
        type: 'text',
        textDirection: 'ltr',
        labelDirection: 'rtl',
        placeholderDirection: 'rtl',
        placeholder: 'نام محصول',
        name: 'name',
        value: '',
        errors: [],
        appendElement: false
    });
    const [name, setName] = useState<string>('');
    const [createLoading, setCreateLoading] = useState<boolean>(false);

    const getName = (value: string) => {
        setName(value);

        setFormData({
            ...formData,
            'ProductTitle': value,
        });
    };

    const [priceProps, setPriceProps] = useState<object>({
        label: 'قیمت محصول',
        type: 'text',
        textDirection: 'ltr',
        labelDirection: 'rtl',
        placeholderDirection: 'rtl',
        placeholder: 'قیمت محصول',
        name: 'price',
        value: '',
        errors: [],
        appendElement: false
    });
    const [price, setPrice] = useState<number>(0);

    const getPrice = (value: number) => {
        setPrice(+value);

        setFormData({
            ...formData,
            'ProductPrice': value,
        });
    };


    const [descriptionProps, setDescriptionProps] = useState<object>({
        label: 'توضیحات',
        type: 'text',
        textDirection: 'ltr',
        labelDirection: 'rtl',
        placeholderDirection: 'rtl',
        placeholder: 'توضیحات',
        name: 'description',
        value: '',
        errors: [],
        appendElement: false,
        isTextarea: true
    });
    const [description, setDescription] = useState<string>('');

    const getDescription = (value: string) => {
        setDescription(value);

        setFormData({
            ...formData,
            'Description': value,
        });
    };

    const [file, setFile] = useState<string>('');
    const [fileUrl, setFileUrl] = useState<string>('');

    const setProductFile = (event: any) => {




        setFormData({
            ...formData,
            file:event.target.files[0]
        });
    };

    const closeModal = () => {
        setShowModal(false)
    }

    const openModal = () => {
        setShowModal(true)
    }

    const [showModal, setShowModal] = useState(false);


    const {data, error, mutateAsync} = useMutation({
        mutationFn: addProduct
    });

    const logout = () =>{
        console.log('helloooo')
        deleteCookie('accessToken');
        deleteCookie('refreshToken');
        localStorage.removeItem('userData')
        router.push('/')
    }

    const createItem = async (event: any) => {
        event.preventDefault()

        let formParams = new FormData();
        formParams.append("file", formData?.file);
        formParams.append("ProductTitle", formData?.ProductTitle );
        formParams.append("Description",  formData?.Description);
        formParams.append("ProductPrice", formData?.ProductPrice);



        setCreateLoading(true);

        try {
            const data = await mutateAsync(formParams); // Trigger the mutation

            if (data.hasError) {
                setCreateLoading(false);
                // setErrorMessage(data.error);
                // setIsSuccess(false);
                // setSuccessMessage('');
            } else {
                setCreateLoading(false);
                // setErrorMessage(data.error);
                // setIsSuccess(true);
                setSuccessMessage('محصول جدید با موفقیت اضافه شد.');
                closeModal()
            }

        } catch (error) {
            setCreateLoading(false);
            setIsSuccess(false);
            console.error('Error submitting form:', error);
            // Handle form submission errors
        }


    }

    return (

        <div>


            {(showModal) &&
                <div className="absolute inset-0 w-full h-full flex justify-center">
                    <div className="opacity-50 dark:opacity-70 fixed inset-0 z-[30] bg-black-100 dark:bg-black-300"
                         onClick={closeModal}></div>

                    <div className="flex justify-center w-full items-end md:items-center">

                        <div
                            className="rounded-lg w-full md:w-[40rem] overflow-x-hidden overflow-y-auto h-fit items-center justify-center inset-0 z-[40] dark:border dark:border-black-500 relative flex flex-col bg-white-100 dark:bg-black-300 outline-none focus:outline-none"
                        >
                            <div className="flex items-center justify-end w-full rounded-t-2xl">

                                <button
                                    className="text-[#454545] bg-transparent rounded outline-none focus:outline-none ease-linear transition-all duration-150"
                                    type="button"
                                    onClick={closeModal}>

                                    <svg
                                        width="40" height="40" viewBox="0 0 48 48" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="24" cy="24" r="24" transform="matrix(-1 0 0 1 48 0)"
                                                fill="white"/>
                                        <path
                                            d="M24.0006 22.8221L28.1253 18.6973L29.3038 19.8758L25.1791 24.0006L29.3038 28.1253L28.1253 29.3038L24.0006 25.1791L19.8758 29.3038L18.6973 28.1253L22.8221 24.0006L18.6973 19.8758L19.8758 18.6973L24.0006 22.8221Z"
                                            fill="#454545"/>
                                    </svg>

                                </button>


                            </div>


                            <div className="relative px-5 pb-5 w-full md:w-[60%] flex flex-col items-center justify-center">

                                <h3 className="w-full text-xs text-black-200 text-right mb-4">
                                    افزودن محصول
                                </h3>

                                <form onSubmit={createItem}
                                      className='w-full'>
                                    <div className="relative mt-5 w-full text-[#A0A0A0]">

                                        <Input inputProps={nameProps}
                                               value={name}
                                               input-custom-style="w-full rounded-lg bg-white-100"
                                               borderInput='border border-[#9A9A9A]'
                                               inputCustomStyle='rounded-lg'
                                               textDirectionStyle=''

                                               onValueChange={getName}/>

                                    </div>

                                    <div className="relative mt-10 w-full text-[#A0A0A0]">

                                        <Input inputProps={priceProps}
                                               value={price}
                                               input-custom-style="w-full rounded-lg bg-white-100"
                                               borderInput='border border-[#9A9A9A]'
                                               inputCustomStyle='rounded-lg'
                                               textDirectionStyle=''
                                               onValueChange={getPrice}/>

                                    </div>

                                    <div className="relative mt-10 w-full text-[#A0A0A0]">
                                        <Input inputProps={descriptionProps}
                                               value={description}
                                               input-custom-style="w-full rounded-lg bg-white-100"
                                               borderInput='border border-[#9A9A9A]'
                                               inputCustomStyle='rounded-lg'
                                               textDirectionStyle=''

                                               onValueChange={getDescription}
                                        />
                                    </div>

                                    <div className="relative mt-10 w-full text-[#A0A0A0]">

                                        <div className="flex items-center justify-between w-full rounded-md h-[30px]">
                                            {(file)
                                                ?
                                                <span
                                                    className="flex items-center justify-start pr-3 h-full rounded-md text-xs w-full border border-[#B6B6B6] border-l-transparent">{fileUrl}</span>
                                                :
                                                <span
                                                    className="flex items-center justify-start pr-3 h-full rounded-md text-xs w-full border border-[#B6B6B6] border-l-transparent">فایل مورد نظر را انتخاب کنید.</span>}

                                            <div
                                                className="mr-[-7px] h-full bg-[#C9C9C9] relative w-[40%] flex items-center justify-center rounded-md">
                                                <input type="file"
                                                       accept="image/x-png,image/gif,image/jpeg"
                                                       className="absolute right-0 top-0 w-full h-full z-[2] opacity-0 cursor-pointer"
                                                       onChange={setProductFile}/>
                                                <span className="text-xs text-[#5C5C5C]">انتخاب فایل</span>
                                            </div>

                                        </div>
                                    </div>

                                    <div className='flex items-center'>
                                        <button type='button'
                                                onClick={closeModal}
                                                className="mt-10 p-3 rounded-lg bg-transparent text-[#5C5C5C] w-full flex items-center justify-center hover:opacity-80 shadow-none transition-all
                    focus:outline-none focus:shadow-none active:outline-none active:shadow-none font-medium text-sm
                    text-center">
                                            <div className="flex items-center w-full justify-center">

                                                <span className="w-full">انصراف</span>

                                            </div>

                                        </button>

                                        <button type='submit'
                                                disabled={createLoading ? true : false}
                                                className="mt-10 p-3 rounded-lg bg-[#46B666] text-white-100 w-full flex items-center justify-center hover:opacity-80 shadow-none transition-all
                    focus:outline-none focus:shadow-none active:outline-none active:shadow-none font-medium text-sm
                    text-center">
                                            {createLoading ?
                                                <svg aria-hidden="true"
                                                     role="status"
                                                     className="inline w-4 h-4 mr-3 text-white-100 animate-spin"
                                                     viewBox="0 0 100 101"
                                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"/>
                                                    <path
                                                        d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                                                        fill="currentColor"/>
                                                </svg>
                                                :
                                                <div className="flex items-center w-full justify-center">

                                                    <span className="w-full">ثبت محصول</span>

                                                </div>
                                            }


                                        </button>


                                    </div>

                                </form>


                            </div>


                        </div>


                    </div>

                </div>
            }


            <nav className='flex items-center justify-between text-xs border-b border-[#A0A0A0] pb-3'>


                <h1 className='block text-[#5C5C5C]'>لیست محصولات</h1>
                <div className='flex items-center'>

                    <button type='button'
                            onClick={openModal}
                            className="whitespace-nowrap w-full px-10 py-2 rounded-lg bg-[#46B666] text-white-100 flex items-center justify-center hover:opacity-80 shadow-none transition-all
                    focus:outline-none focus:shadow-none active:outline-none active:shadow-none
                    text-center">

                        <img src="/assets/img/plus.svg"
                             className="w-[15px]"/>
                        <span className="w-full mr-1">افزودن محصول</span>

                    </button>

                    <div className='w-full hidden md:block mr-4 text-[#5C5C5C]'>
                        <span>{userName}</span>
                    </div>

                    <div onClick={logout} className='w-full hidden md:flex items-center justify-end mr-1 cursor-pointer'>
                        <img src="/assets/img/logout.svg"
                             className="w-[20px]"/>
                        <span className='text-danger-200 mr-1' >خروج</span>
                    </div>


                </div>


            </nav>


        </div>

    )
}

export default Navbar;