import httpService from "../server/httpService";

export async function login(data: any) {
    const response = await httpService.post('/api/Security/UserLogin/Login', data);
    return response.data;
}

export async function getList() {
    return httpService.get('/api/General/Product/ProductList').then((data) => data.data)
}

export async function addProduct(data: any) {
    return httpService.post('/api/General/Product/AddProduct', data).then((data) => data.data)
}
