'use client'

import ReactQueryProvider from './providers/reactQueryProvider'
import '../assets/css/globals.css'


import React, {FC} from 'react';

import ReduxProvider from './providers/ReduxProvider';

export default function RootLayout({
                                       children,
                                   }: {
    children: React.ReactNode
}) {


    return (
        <html lang="en">
        <head>
            <title>های وب</title>
        </head>
        <body>

        <ReduxProvider>
            <ReactQueryProvider>

                {children}

            </ReactQueryProvider>
        </ReduxProvider>

        </body>
        </html>
    )
}