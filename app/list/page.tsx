'use client'

import ListLayout from '../../components/layouts/ListLayout';


import React, {useEffect, useState} from 'react';

import {getList} from "../../api";
import {useMutation} from '@tanstack/react-query';

import Pagination from '../../components/base/Pagination';
import {getProducts} from "../store/products";

import {useDispatch, useSelector} from 'react-redux';
import store from '../store/store';

export default function Page() {
    // const dispatch = useDispatch();

    const {data, error, isPending, isSuccess, mutateAsync} = useMutation({
        mutationFn: getList
    });
    const [productList, setProductList] = useState<any[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(true)

    const fetchList = async (value: any) => {
        try {
            const {data} = await mutateAsync(); // Trigger the mutation
            setProductList(data.list)
            setIsLoading(false)
        } catch (error) {
            console.error('Error submitting form:', error);
            // Handle form submission errors
        }
    }

    const stateValue = useSelector((state: GlobalState) => state.propertyFromState); // Replace with your types

    useEffect(() => {
        setIsLoading(true)

        // @ts-ignore
        // dispatch(getProducts());
        fetchList(10);
        // fetchList(10);
        document.title = 'لیست محصولات'
    }, [])


    const itemsPerPage: number = 10; // Adjust as needed
    const [currentPage, setCurrentPage] = useState<number>(1);

    const handlePageChange = (pageNumber: number) => {
        setCurrentPage(pageNumber);
        // @ts-ignore
        // dispatch(getProducts(pageNumber));
        fetchList(pageNumber);
    };

    const paginatedData = productList.slice((currentPage - 1) * itemsPerPage, currentPage * itemsPerPage);


    return (
        <ListLayout>


            {(isLoading) ?
                <div className='flex items-center justify-center w-full h-full mt-52'>
                    <svg viewBox="0 0 40 40"
                         fill="none"
                         xmlns="http://www.w3.org/2000/svg"
                         className="w-7 h-7 text-white-100 animate-spin">
                        <path
                            d="M21.5 2V8C21.5 8.39782 21.342 8.77936 21.0607 9.06066C20.7794 9.34196 20.3978 9.5 20 9.5C19.6022 9.5 19.2206 9.34196 18.9393 9.06066C18.658 8.77936 18.5 8.39782 18.5 8V2C18.5 1.60218 18.658 1.22064 18.9393 0.93934C19.2206 0.658035 19.6022 0.5 20 0.5C20.3978 0.5 20.7794 0.658035 21.0607 0.93934C21.342 1.22064 21.5 1.60218 21.5 2ZM28.4844 13.0156C28.6815 13.0155 28.8768 12.9766 29.0589 12.901C29.241 12.8254 29.4064 12.7146 29.5456 12.575L33.7887 8.33375C34.0702 8.05229 34.2283 7.67055 34.2283 7.2725C34.2283 6.87445 34.0702 6.49271 33.7887 6.21125C33.5073 5.92979 33.1255 5.77167 32.7275 5.77167C32.3295 5.77167 31.9477 5.92979 31.6663 6.21125L27.425 10.4544C27.2151 10.664 27.0721 10.9313 27.014 11.2222C26.956 11.5132 26.9856 11.8148 27.099 12.0889C27.2124 12.3631 27.4045 12.5975 27.6511 12.7624C27.8977 12.9274 28.1877 13.0155 28.4844 13.0156ZM38 18.5H32C31.6022 18.5 31.2206 18.658 30.9393 18.9393C30.658 19.2206 30.5 19.6022 30.5 20C30.5 20.3978 30.658 20.7794 30.9393 21.0607C31.2206 21.342 31.6022 21.5 32 21.5H38C38.3978 21.5 38.7794 21.342 39.0607 21.0607C39.342 20.7794 39.5 20.3978 39.5 20C39.5 19.6022 39.342 19.2206 39.0607 18.9393C38.7794 18.658 38.3978 18.5 38 18.5ZM29.5456 27.425C29.262 27.1556 28.8844 27.0076 28.4933 27.0126C28.1021 27.0176 27.7284 27.1752 27.4518 27.4518C27.1752 27.7284 27.0176 28.1021 27.0126 28.4933C27.0076 28.8844 27.1556 29.262 27.425 29.5456L31.6663 33.7887C31.9477 34.0702 32.3295 34.2283 32.7275 34.2283C33.1255 34.2283 33.5073 34.0702 33.7887 33.7887C34.0702 33.5073 34.2283 33.1255 34.2283 32.7275C34.2283 32.3295 34.0702 31.9477 33.7887 31.6663L29.5456 27.425ZM20 30.5C19.6022 30.5 19.2206 30.658 18.9393 30.9393C18.658 31.2206 18.5 31.6022 18.5 32V38C18.5 38.3978 18.658 38.7794 18.9393 39.0607C19.2206 39.342 19.6022 39.5 20 39.5C20.3978 39.5 20.7794 39.342 21.0607 39.0607C21.342 38.7794 21.5 38.3978 21.5 38V32C21.5 31.6022 21.342 31.2206 21.0607 30.9393C20.7794 30.658 20.3978 30.5 20 30.5ZM10.4544 27.425L6.21125 31.6663C5.92979 31.9477 5.77167 32.3295 5.77167 32.7275C5.77167 33.1255 5.92979 33.5073 6.21125 33.7887C6.49271 34.0702 6.87445 34.2283 7.2725 34.2283C7.67055 34.2283 8.05229 34.0702 8.33375 33.7887L12.575 29.5456C12.8444 29.262 12.9924 28.8844 12.9874 28.4933C12.9824 28.1021 12.8248 27.7284 12.5482 27.4518C12.2716 27.1752 11.8979 27.0176 11.5067 27.0126C11.1156 27.0076 10.738 27.1556 10.4544 27.425ZM9.5 20C9.5 19.6022 9.34196 19.2206 9.06066 18.9393C8.77936 18.658 8.39782 18.5 8 18.5H2C1.60218 18.5 1.22064 18.658 0.93934 18.9393C0.658035 19.2206 0.5 19.6022 0.5 20C0.5 20.3978 0.658035 20.7794 0.93934 21.0607C1.22064 21.342 1.60218 21.5 2 21.5H8C8.39782 21.5 8.77936 21.342 9.06066 21.0607C9.34196 20.7794 9.5 20.3978 9.5 20ZM8.33375 6.21125C8.05229 5.92979 7.67055 5.77167 7.2725 5.77167C6.87445 5.77167 6.49271 5.92979 6.21125 6.21125C5.92979 6.49271 5.77167 6.87445 5.77167 7.2725C5.77167 7.67055 5.92979 8.05229 6.21125 8.33375L10.4544 12.575C10.738 12.8444 11.1156 12.9924 11.5067 12.9874C11.8979 12.9824 12.2716 12.8248 12.5482 12.5482C12.8248 12.2716 12.9824 11.8979 12.9874 11.5067C12.9924 11.1156 12.8444 10.738 12.575 10.4544L8.33375 6.21125Z"
                            fill="#5C5C5C"/>
                    </svg>
                </div>
                :
                <>
                    {(productList.length > 0)
                        ?
                        (
                            <div>
                                <div className="grid grid-flow-row-dense grid-cols-1 md:grid-cols-4 mx-auto gap-x-10 pt-10">

                                    {productList.map((item) => {
                                        return (
                                            <div key={item.id}
                                                 className="w-full shadow-[0_4px_10px_rgba(0,0,0,0.1)] mb-4 bg-[#fff] rounded-xl flex flex-col">

                                                <img src={item.imageUrl}
                                                     className="w-full rounded-t-xl"/>

                                                <div className='flex flex-col p-5'>
                                                    <h3 className='text-black-100 text-md mb-3'>{item.title}</h3>

                                                    <span className='text-xs text-[#5C5C5C] mb-4'>
                                        {item.description}

                        </span>

                                                    <div className='flex items-center'>
                        <span className='text-xs text-[#5C5C5C] ml-1'>
                            قیمت:
                        </span>
                                                        <span className='text-xs text-black-100'>
                            {item.price}
                        </span>
                                                    </div>

                                                </div>

                                            </div>

                                        )
                                    })}

                                </div>
                                <div className='w-full flex justify-center md:justify-end'>
                                    <Pagination
                                        currentPage={currentPage}
                                        totalPages={Math.ceil(productList.length / itemsPerPage)}
                                        onPageChange={handlePageChange}
                                    />
                                </div>
                            </div>

                        )
                        :

                        <div className='flex items-center justify-center w-full h-full'>
                            <img src="/assets/img/empty-list.svg"
                                 className="w-fit"/>
                        </div>
                    }
                </>

            }


        </ListLayout>
    )
}