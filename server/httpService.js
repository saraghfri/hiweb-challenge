import axios from "axios";

import {getCookie} from "cookies-next";

const app = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_BASE_URL,
    withCredentials: true
})
app.interceptors.request.use(
    config => {

        if (getCookie('accessToken')) {
            config.headers.Authorization = `Bearer ${getCookie('accessToken')}`
        }
        return config
    },
    error => Promise.reject(error)
)
app.interceptors.response.use(
    res => res,
    async (err) => {
        const originalConfig = err.config;
        if (err.response.status === 401 && !originalConfig._retry) {
            originalConfig._retry = true
            try {
                let name = ''
                if(localStorage.getItem('userData')){
                    const {userName} = JSON.parse(localStorage.getItem('userData'))
                    name = userName
                }
                const {data} = await axios.post(`${process.env.NEXT_PUBLIC_API_BASE_URL}/api/Security/UserLogin/RefreshToken`, {
                    userName: name,
                    refreshToken: getCookie('refreshToken')
                })
                if (data) return app(originalConfig)
            } catch (e) {
                return Promise.reject(e)
            }
        }
        return Promise.reject(err)
    }
)

const http = {
    get: app.get,
    post: app.post,
    delete: app.delete,
    put: app.put,
    patch: app.patch,
}

export default http;