const initialState = {
    list: [],
    loading: false,
    error: null,
};

import {
    FETCH_LIST_REQUEST,
    FETCH_LIST_SUCCESS,
    FETCH_LIST_FAILURE,
} from './actionTypes';

const rootReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case FETCH_LIST_REQUEST:
            return {
                ...state,
                loading: true,
                error: null,
            };
        case FETCH_LIST_SUCCESS:
            return {
                ...state,
                loading: false,
                list: action.payload,
            };
        case FETCH_LIST_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload,
            };
        default:
            return state;
    }
};

// export default rootReducer;

import { configureStore } from '@reduxjs/toolkit';

const store = configureStore({
    reducer: rootReducer,
});

export default store;