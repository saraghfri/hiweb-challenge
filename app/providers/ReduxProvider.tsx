import { Provider } from 'react-redux';
import store from '../store/store';
import React from "react";

type ReduxProviderProps = { children: React.ReactNode };

function ReduxProvider({ children }: ReduxProviderProps) {
    // @ts-ignore
    return <Provider store={store}>{children}</Provider>;
}

export default ReduxProvider;