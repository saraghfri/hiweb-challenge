const LoginLayout = ({children}) => {
    return (
        <div className="h-screen grid grid-cols-1 md:grid-cols-2">


            <div className="hidden md:flex bg-white-100 flex-col items-center justify-center">
                <img src="/assets/img/login.svg"
                     className="w-[700px]"/>
            </div>

            <div
                className="w-full flex justify-center items-center overflow-y-hidden md:overflow-y-auto">
                <section className="text-right flex justify-center w-full h-full">
                    <div className="gap-y-20 flex flex-col items-center justify-center h-full w-full md:w-auto">
                        <div>
                            <img src="/assets/img/logo.svg"
                                 className='w-[100px]'/>
                        </div>
                        <div>
                            {children}
                        </div>
                    </div>
                </section>
            </div>

        </div>
    );
};
export default LoginLayout;