import Navbar from "../Navbar";

const ListLayout = ({ children }) => {
    return (
        <div className='py-10 px-5 md:px-20 flex flex-col'>
            <Navbar></Navbar>
            {children}
        </div>
    );
};
export default ListLayout;