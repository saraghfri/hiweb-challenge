'use client'
import React from 'react';
import {useState} from "react";

interface props {
    borderInput: string;
    inputCustomStyle: string;
    inputProps: object;
    textDirectionStyle: string;
    value: string;
    onValueChange: (value: string) => void;
}


const Input: React.FC<props> = ({
                                    value,
                                    textDirectionStyle,
                                    borderInput,
                                    inputCustomStyle,
                                    inputProps = {
                                        minLength: null,
                                        maxLength: null,
                                        option: 'default',
                                        label: '',
                                        placeholder: '',
                                        type: 'text',
                                        labelDirection: 'rtl',
                                        textDirection: 'rtl',
                                        placeholderDirection: 'rtl',
                                        name: '',
                                        errors: [],
                                        disabled: false,
                                        appendIcon: '',
                                        prependIcon: '',
                                        preIcon: '',
                                        preElement: false,
                                        appendElement: false,
                                        prependElement: false,
                                        required: false,
                                        readonly: false,
                                        hint: [],
                                        hasMask: false,
                                        isDecimalNumber: false,
                                        autoFocus: false,
                                        isTextarea: false
                                    },
                                    onValueChange
                                }) => {


    const [inputVal, setInputVal] = useState('');
    const [noFocus, setNoFocus] = useState(true);
    const [isTransitionTitleToTop, setIsTransitionTitleToTop] = useState(false);

    // Function to handle input change
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInputVal(event.target.value); // Update the input value state
        onValueChange(event.target.value);
    };

    const blurHandler = (event: any) => {
        if (inputVal?.length < 1) {
            setIsTransitionTitleToTop(false)
        }

        setNoFocus(true)
        event.target.blur()
        // emit('emitBlur')
    }

    const focusHandler = (event: any) => {
        setIsTransitionTitleToTop(true)
        event.target.focus()
        setNoFocus(false)
    }


    // const transitionClass = () => {
    //
    //     console.log(inputProps?.labelDirection,'inputProps?.labelDirection')
    //     const labelPos = inputProps?.labelDirection === 'ltr' ? `left-2.5` : 'right-2.5'
    //
    //     if (isTransitionTitleToTop) {
    //         return `${labelPos} px-2 -top-2 overline-1`
    //     }
    //
    //     return `top-2.5 body-2 `
    // }

    return (

        <div>
            <div className="block">

                <div className={`relative group flex items-center border ${borderInput} ${inputCustomStyle}`}>
                    <div className="relative flex items-center space-x-2 w-full">

                        {(inputProps.isTextarea) ?
                            <textarea id={`${inputProps?.value}-${inputProps?.type}`}
                                      type={inputProps?.type}
                                      name={inputProps?.name}
                                      dir={inputProps?.labelDirection}
                                      placeholder={!isTransitionTitleToTop ? '' : inputProps?.placeholder}
                                      disabled={inputProps?.disabled}
                                      required={inputProps?.required}
                                      className={`${textDirectionStyle} ${inputCustomStyle} resize-none p-2.5 w-full h-20 text-xs transition-all border-none outline-none shadow-none placeholder:text-right`}
                                      onBlur={blurHandler}
                                      onFocus={focusHandler}
                                      onChange={handleChange}>
                                {inputProps.label}
                            </textarea>
                            :
                            <input
                                id={`${inputProps?.value}-${inputProps?.type}`}
                                type={inputProps?.type}
                                name={inputProps?.name}
                                dir={inputProps?.labelDirection}
                                placeholder={!isTransitionTitleToTop ? '' : inputProps?.placeholder}
                                disabled={inputProps?.disabled}
                                required={inputProps?.required}
                                className={`${textDirectionStyle} ${inputCustomStyle} p-2.5 w-full h-10 text-xs transition-all border-none outline-none shadow-none placeholder:text-right`}
                                onBlur={blurHandler}
                                onFocus={focusHandler}
                                onChange={handleChange}
                            />
                        }


                    </div>
                </div>

                <label
                    className={`z-[2] bg-white-100 text-xs text-black-300 transition-all absolute cursor-text px-2 -top-2 overline-1 right-2.5`}>
                    {isTransitionTitleToTop ? inputProps?.label : inputProps?.placeholder}
                </label>

                {(inputProps?.errors) &&
                    <div className="text-right pr-1">
                        <ul className={`transition-all duration-300 overline-1 ${(inputProps?.errors.length > 0) && 'show'}`}>

                            {(inputProps?.errors).map((error: any) => (
                                <li key={error.id}>
                                    <span className="text-danger-500 text-xs">
                                        {error?.message}
                                    </span>
                                </li>
                            ))}

                        </ul>
                    </div>
                }

            </div>
        </div>
    )

};

export default Input;