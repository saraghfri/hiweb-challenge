'use client'

import Input from "../../components/base/Input";
import LoginLayout from '../../components/layouts/LoginLayout';

import React, {useEffect} from 'react';
import {useState} from "react";

import {login} from '../../api';

import {useMutation} from '@tanstack/react-query';
import {useRouter} from 'next/navigation';

import {setCookie} from 'cookies-next';

export default function Page() {
    const router = useRouter();

    const [formData, setFormData] = useState({
        userName: '',
        passWord: '',
    });
    useEffect(() => {
        document.title = 'ورود به حساب'
    }, []);

    const [errorMessage, setErrorMessage] = useState<string | null>(null);
    const [successMessage, setSuccessMessage] = useState<string | null>(null);

    const [loginLoading, setLoginLoading] = useState<boolean>(false);


    const [usernameProps, setUsernameProps] = useState<object>({
        label: 'نام کاربری',
        type: 'text',
        textDirection: 'ltr',
        labelDirection: 'rtl',
        placeholderDirection: 'rtl',
        placeholder: 'نام کاربری خود را وارد کنید',
        name: 'userName',
        value: '',
        errors: [],
        appendElement: false
    });
    const [username, setUsername] = useState<string>('');

    const [passwordProps, setPasswordProps] = useState<object>({
        label: 'رمز عبور',
        type: 'password',
        textDirection: 'ltr',
        labelDirection: 'rtl',
        placeholderDirection: 'rtl',
        placeholder: 'رمز عبور خود را وارد کنید',
        name: 'password',
        value: '',
        errors: [],
        appendElement: false
    });
    const [password, setPassword] = useState<string>('');

    const [disabled, setDisabled] = useState<boolean>(false);
    // const [loginLoading, setLoginLoading] = useState<boolean>(false);


    const [isSuccess, setIsSuccess] = useState<boolean>(false);

    const getUsername = (value: string) => {
        setUsername(value);

        setFormData({
            ...formData,
            'userName': value,
        });

    };

    const getPassword = (value: string) => {
        setPassword(value);

        setFormData({
            ...formData,
            'passWord': value,
        });

    };


    const {data, error, mutateAsync} = useMutation({
        mutationFn: login
    });

    const handleLogin = async (event: any) => {
        setIsSuccess(false);

        event.preventDefault();

        setLoginLoading(true);

        try {
            const response = await mutateAsync(formData); // Trigger the mutation


            if (response.hasError) {
                setLoginLoading(false);
                setErrorMessage(response.error);
                setIsSuccess(false);
                setSuccessMessage('');
            } else {
                setLoginLoading(false);
                setErrorMessage('');
                setIsSuccess(true);
                setSuccessMessage('ورود شما با موفقیت انجام شد.');

                setCookie(
                    'accessToken',
                    (response.data.accessToken.access_token),
                    {maxAge: response.data.accessToken.expires_in}
                );


                setCookie(
                    'refreshToken',
                    (response.data.accessToken.refresh_token),
                    {expires: new Date(response.data.accessToken.expire_refresh_token)}
                );


                const userData = {
                    token_type: response.data.accessToken.token_type,
                    userName: response.data.userName
                }
                localStorage.setItem('userData', JSON.stringify(userData));


                router.push('/list');

            }

        } catch (er: any) {

            setErrorMessage(er.response.data.error);
            setLoginLoading(false);
            setIsSuccess(false);
        }

    };


    return (
        <>
            <LoginLayout>

                {(errorMessage) &&
                    <span className='text-red-500 text-sm text-center w-full block'>{errorMessage}</span>
                }

                <div className='w-full h-full flex justify-center items-center'>

                    {(!isSuccess) ?

                        <div className='w-full md:w-[400px] border border-[#9A9A9A] rounded-lg p-10'>
                            <form onSubmit={handleLogin}>
                                <div className="relative mt-5 w-full text-[#9A9A9A]">

                                    <Input inputProps={usernameProps}
                                           value={username}
                                           input-custom-style="rounded-lg bg-white-100"
                                           borderInput='border border-[#9A9A9A]'
                                           inputCustomStyle='rounded-lg'
                                           textDirectionStyle=''

                                           onValueChange={getUsername}/>

                                </div>

                                <div className="relative mt-10 w-full text-[#9A9A9A]">

                                    <Input inputProps={passwordProps}
                                           value={password}
                                           input-custom-style="rounded-lg bg-white-100"
                                           borderInput='border border-[#9A9A9A]'
                                           inputCustomStyle='rounded-lg'
                                           textDirectionStyle=''

                                           onValueChange={getPassword}/>

                                </div>

                                <div className="flex gap-10 mt-4">

                                    <div className="flex items-center w-full text-[#989898]">
                                        <label
                                            className="relative flex cursor-pointer items-center h-[18px] w-[18px]"
                                            data-ripple-dark="true">
                                            <input
                                                name="terms"
                                                v-model="terms"
                                                value="false"
                                                type="checkbox"
                                                className="before:content[''] peer relative h-full w-full cursor-pointer appearance-none
                                border rounded-sm border-black-200 transition-all"
                                            />
                                            <div
                                                className="h-full w-full flex items-center justify-center pointer-events-none absolute top-0 left-0 opacity-0 transition-opacity peer-checked:opacity-100">

                                                <svg width='10'
                                                     viewBox="0 0 12 9"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M4.66746 7.11391L10.7957 0.985657L11.7385 1.92846L4.66746 8.99951L0.424805 4.75691L1.36761 3.81411L4.66746 7.11391Z"
                                                        fill="#989898"/>
                                                </svg>


                                            </div>
                                        </label>
                                        <label
                                            className="cursor-pointer select-none font-light text-[#00ACED] text-xs mr-2">
                                            <span>مرا به خاطر بسپار</span>
                                        </label>
                                    </div>
                                </div>


                                <button type='submit'
                                        disabled={disabled || loginLoading ? true : false}
                                        className="mt-10 p-3 rounded-lg bg-[#46B666] text-white-100 w-full flex items-center justify-center hover:opacity-80 shadow-none transition-all
                    focus:outline-none focus:shadow-none active:outline-none active:shadow-none font-medium text-sm
                    text-center">
                                    {loginLoading ?
                                        <svg aria-hidden="true"
                                             role="status"
                                             className="inline w-4 h-4 mr-3 text-white-100 animate-spin"
                                             viewBox="0 0 100 101"
                                             fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"/>
                                            <path
                                                d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                                                fill="currentColor"/>
                                        </svg>
                                        :
                                        <div className="flex items-center w-full justify-center">

                                            <span className="w-full">ورود</span>

                                        </div>
                                    }


                                </button>

                            </form>
                        </div>

                        :
                        <div className='w-full md:w-[400px] border border-[#9A9A9A] rounded-lg p-10'>
                            <div className='flex flex-col items-center gap-y-3'>
                                <img src="/assets/img/success.svg"
                                     className="w-[40px]"/>

                                <span
                                    className='text-primary-100 text-sm text-center w-full block my-5'>{successMessage}</span>

                                <svg viewBox="0 0 40 40"
                                     fill="none"
                                     xmlns="http://www.w3.org/2000/svg"
                                     className="w-7 h-7 text-white-100 animate-spin">
                                    <path
                                        d="M21.5 2V8C21.5 8.39782 21.342 8.77936 21.0607 9.06066C20.7794 9.34196 20.3978 9.5 20 9.5C19.6022 9.5 19.2206 9.34196 18.9393 9.06066C18.658 8.77936 18.5 8.39782 18.5 8V2C18.5 1.60218 18.658 1.22064 18.9393 0.93934C19.2206 0.658035 19.6022 0.5 20 0.5C20.3978 0.5 20.7794 0.658035 21.0607 0.93934C21.342 1.22064 21.5 1.60218 21.5 2ZM28.4844 13.0156C28.6815 13.0155 28.8768 12.9766 29.0589 12.901C29.241 12.8254 29.4064 12.7146 29.5456 12.575L33.7887 8.33375C34.0702 8.05229 34.2283 7.67055 34.2283 7.2725C34.2283 6.87445 34.0702 6.49271 33.7887 6.21125C33.5073 5.92979 33.1255 5.77167 32.7275 5.77167C32.3295 5.77167 31.9477 5.92979 31.6663 6.21125L27.425 10.4544C27.2151 10.664 27.0721 10.9313 27.014 11.2222C26.956 11.5132 26.9856 11.8148 27.099 12.0889C27.2124 12.3631 27.4045 12.5975 27.6511 12.7624C27.8977 12.9274 28.1877 13.0155 28.4844 13.0156ZM38 18.5H32C31.6022 18.5 31.2206 18.658 30.9393 18.9393C30.658 19.2206 30.5 19.6022 30.5 20C30.5 20.3978 30.658 20.7794 30.9393 21.0607C31.2206 21.342 31.6022 21.5 32 21.5H38C38.3978 21.5 38.7794 21.342 39.0607 21.0607C39.342 20.7794 39.5 20.3978 39.5 20C39.5 19.6022 39.342 19.2206 39.0607 18.9393C38.7794 18.658 38.3978 18.5 38 18.5ZM29.5456 27.425C29.262 27.1556 28.8844 27.0076 28.4933 27.0126C28.1021 27.0176 27.7284 27.1752 27.4518 27.4518C27.1752 27.7284 27.0176 28.1021 27.0126 28.4933C27.0076 28.8844 27.1556 29.262 27.425 29.5456L31.6663 33.7887C31.9477 34.0702 32.3295 34.2283 32.7275 34.2283C33.1255 34.2283 33.5073 34.0702 33.7887 33.7887C34.0702 33.5073 34.2283 33.1255 34.2283 32.7275C34.2283 32.3295 34.0702 31.9477 33.7887 31.6663L29.5456 27.425ZM20 30.5C19.6022 30.5 19.2206 30.658 18.9393 30.9393C18.658 31.2206 18.5 31.6022 18.5 32V38C18.5 38.3978 18.658 38.7794 18.9393 39.0607C19.2206 39.342 19.6022 39.5 20 39.5C20.3978 39.5 20.7794 39.342 21.0607 39.0607C21.342 38.7794 21.5 38.3978 21.5 38V32C21.5 31.6022 21.342 31.2206 21.0607 30.9393C20.7794 30.658 20.3978 30.5 20 30.5ZM10.4544 27.425L6.21125 31.6663C5.92979 31.9477 5.77167 32.3295 5.77167 32.7275C5.77167 33.1255 5.92979 33.5073 6.21125 33.7887C6.49271 34.0702 6.87445 34.2283 7.2725 34.2283C7.67055 34.2283 8.05229 34.0702 8.33375 33.7887L12.575 29.5456C12.8444 29.262 12.9924 28.8844 12.9874 28.4933C12.9824 28.1021 12.8248 27.7284 12.5482 27.4518C12.2716 27.1752 11.8979 27.0176 11.5067 27.0126C11.1156 27.0076 10.738 27.1556 10.4544 27.425ZM9.5 20C9.5 19.6022 9.34196 19.2206 9.06066 18.9393C8.77936 18.658 8.39782 18.5 8 18.5H2C1.60218 18.5 1.22064 18.658 0.93934 18.9393C0.658035 19.2206 0.5 19.6022 0.5 20C0.5 20.3978 0.658035 20.7794 0.93934 21.0607C1.22064 21.342 1.60218 21.5 2 21.5H8C8.39782 21.5 8.77936 21.342 9.06066 21.0607C9.34196 20.7794 9.5 20.3978 9.5 20ZM8.33375 6.21125C8.05229 5.92979 7.67055 5.77167 7.2725 5.77167C6.87445 5.77167 6.49271 5.92979 6.21125 6.21125C5.92979 6.49271 5.77167 6.87445 5.77167 7.2725C5.77167 7.67055 5.92979 8.05229 6.21125 8.33375L10.4544 12.575C10.738 12.8444 11.1156 12.9924 11.5067 12.9874C11.8979 12.9824 12.2716 12.8248 12.5482 12.5482C12.8248 12.2716 12.9824 11.8979 12.9874 11.5067C12.9924 11.1156 12.8444 10.738 12.575 10.4544L8.33375 6.21125Z"
                                        fill="#5C5C5C"/>
                                </svg>

                            </div>
                        </div>
                    }


                </div>
            </LoginLayout>
        </>
    )
}